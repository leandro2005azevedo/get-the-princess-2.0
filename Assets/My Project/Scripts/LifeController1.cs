using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeController1 : MonoBehaviour
{
    public GameObject playerController;
    public Image life;
    public float currentLife;
    public float maxLife;
    public bool dead;
    

    
    private void Start()
    {
        dead = false;
        currentLife = maxLife;
        LifeUpdate();
    }
  


    public void ReviceHealing(float _heal)
    {
        currentLife = Mathf.Min(currentLife + _heal, 0f);
        LifeUpdate();
    }
    public void TakesDamege(float _damage)
    {
        currentLife = Mathf.Max(currentLife - _damage, 0f);
        if(currentLife == 0)
        {
            dead = true;
        }
        LifeUpdate();
    }
   
    public void LifeUpdate()
    {
        life.fillAmount = currentLife / maxLife;
    }
}
