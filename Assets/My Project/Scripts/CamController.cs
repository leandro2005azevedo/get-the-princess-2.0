using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    private float center;
    [SerializeField] private float minX, maxX;
    public Transform p1, p2;


    private void Update()
    {

        center = (p1.position.x + p2.position.x) / 2;
        Vector3 _pos = transform.position;
        _pos.x = Mathf.Clamp(center, minX, maxX);
        transform.position = _pos;

    }








    /*private float center;
    [SerializeField] private float minX, maxX;
    public Transform p1;

    float distancia;

    private void Start()
    {
        distancia = transform.position.x - p1.position.x;
    }

    private void Update()
    {
        if (p1 != null)
        {
            Vector3 _pos = transform.position;
            _pos.x = p1.position.x + distancia;
            transform.position = _pos;

        }
 

    }
    */
}
