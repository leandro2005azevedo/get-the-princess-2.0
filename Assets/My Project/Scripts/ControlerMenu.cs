using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlerMenu : MonoBehaviour
{
    public void Button_Lobby()
    {
        SceneManager.LoadScene("Lobby");
    }
    public void Button_Game1()
    {
        SceneManager.LoadScene("Game");
    }
    public void Button_Game2()
    {
        SceneManager.LoadScene("Game2");
    }

    public void Button_Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void Button_Exit()
    {
        Application.Quit();
    }
}
