using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController instance;

    public AudioSource DeathP1;
    public AudioSource AttackP1;
    public AudioSource JumpP1;

    public AudioSource JumpP2;
    public AudioSource AttackP2;
    public AudioSource DeathP2;

    private void Awake()
    {
        instance = this;
    }
}
