using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Map : MonoBehaviour
{
    public static Map instance;
    public GameObject menu;
    private bool ativo = false;
    public bool stop = false;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        menu.SetActive(false);
    }
    private void Update()
    {
        OpenMap();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) ativo = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) ativo = false;
    }
   
    public void OpenMap()
    {
        if (Input.GetKeyDown(KeyCode.P) && ativo)
        {
            stop = !stop;
            menu.SetActive(stop);
        }

    }
    public void Button_Game1()
    {
        SceneManager.LoadScene("Game");
    }
    public void Button_Game2()
    {
        SceneManager.LoadScene("Game2");
    }


}
