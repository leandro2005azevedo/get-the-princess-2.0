using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Transform alvo;
    private float distance;
    private Vector2 move;
    public GameObject player1, player2;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float speed;
    [SerializeField] private float alcancedeVisao;
    [SerializeField] private float alcanceataque;
    [SerializeField] private float proximoataque;
    [SerializeField] private float cadenciaataque;
    [SerializeField] private float damege;


    private void Start()
    {
        DefineAlvo();
    }
    private void Update()
    {
        if (GameController.instance.gamePause) return;
        distance = Vector3.Distance(transform.position, alvo.position);

        DefineAlvo();

        if(alvo == null) return;

        if(distance <= alcancedeVisao)
        {
            if(distance <= alcanceataque)
            {
                //atacar
                move = Vector2.zero;
                Atacar();
            }
            else
            {
                //perseguir
                Vector2 _direcao = alvo.position - transform.position;
                _direcao = _direcao.normalized * speed;
                _direcao.y = rb.velocity.y;
                move = _direcao;
            }
        }
        else
        {
            //fica parado
            move = Vector2.zero;
        }
      
    }
    private void FixedUpdate()
    {
        if (GameController.instance.gamePause) return;
        
        Movement();
    }

    void Movement()
    {
        rb.velocity = move;
    }
    void Atacar()
    {
        if(Time.time >= proximoataque)
        {
            Debug.Log("atacar");
            alvo.GetComponent<PlayerController1>().TakesDamage();
            proximoataque = Time.time + cadenciaataque;
        }
    }

    void DefineAlvo()
    {
        
        GameObject[] _alvos = GameObject.FindGameObjectsWithTag("Player");
        float _distance = Mathf.Infinity;
        
        if(_alvos.Length > 0)
        {
            foreach(GameObject _alvo in _alvos)
            {
                float _playerDistance = Vector2.Distance(_alvo.transform.position, transform.position);
                if(_playerDistance < _distance)
                {
                    alvo = _alvo.transform;
                    _distance = _playerDistance;
                }
            }

        }
        else
        {
            alvo = null;
        }

    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Shot"))
        {
            
            Destroy(col.gameObject);
            Destroy(this.gameObject);
        }
    }





}
