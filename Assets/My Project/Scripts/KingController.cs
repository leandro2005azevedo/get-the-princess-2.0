using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DialogueEditor;


public class KingController : MonoBehaviour
{
    public NPCConversation conversation;
    public NPCConversation conversation2;
    bool finalizouconversa = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if(!finalizouconversa)
                ConversationManager.Instance.StartConversation(conversation);
            else
                ConversationManager.Instance.StartConversation(conversation2);
        }
    }

    public void FinalizouConversa()
    {
        finalizouconversa=true;
    }

}
