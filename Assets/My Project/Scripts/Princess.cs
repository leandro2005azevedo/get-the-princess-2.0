using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Princess : MonoBehaviour
{
    public Transform enemys;
    public GameObject tFimFase;

  
    private void Update()
    {
        AchaInimigos();
    }
    void AchaInimigos()
    {
        GameObject[] _enemys = GameObject.FindGameObjectsWithTag("Enemy");
        if (_enemys.Length > 0)
        {
            return;
        }
        else
        {
            enemys = null;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (enemys == null && collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            tFimFase.SetActive(true);
        }
    }
}
