using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance; //singleton
    bool dead1, dead2 = false;
    public GameObject tPause, gameOver, menseger;
    public bool gamePause = false;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
    }
    private void Update()
    {
        Pause();
    }

    public void Pause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gamePause = !gamePause;
            tPause.SetActive(gamePause);

            menseger.SetActive(false);
        }
    }
    public void GameOver(string _player)
    {
        if(_player == "P1")
        {
            dead1 = true;

        }else if(_player == "P2")
        {
            dead2 = true;
        }

        if(dead1 || dead2)
        {
            gameOver.SetActive(true);
        }
    }

}
