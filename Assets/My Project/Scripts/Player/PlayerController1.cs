using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController1 : MonoBehaviour
{
   
    public enum Player
    {
        P1,
        P2,
    }
    public Player player;
    [Header("Movimetação")]
    protected Rigidbody2D rb;
    [SerializeField] private float speed;
    protected float x;
    private bool olhandoEsquerda = false;
    protected Animator animator;
    protected Transform position;

    [Header("Sistema de Pulo")]
    [SerializeField] private float jumpSpeed;
    [SerializeField] private float radius;
    [SerializeField]protected Transform posfoot;
    [SerializeField]protected LayerMask layerGround;
    protected bool jump;

    

    public bool dead = false;

    //Menu//
    private GameObject menu;
    bool menu1;
   
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        position = GetComponent<Transform>(); 
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (GameController.instance.gamePause || dead)
        {
            rb.velocity = Vector3.zero;
            return;
        }

        if (GetComponent<LifeController1>().dead == true)
        {
            dead = true;
            AudioController.instance.DeathP1.Play();
            Death();
        }
        if (dead)
        {
            GameController.instance.gameOver.SetActive(true);
        }
        Inputs();
        Virar();
        
    }
    private void FixedUpdate()
    {
        if (GameController.instance.gamePause || dead ) return;
        Movement();
        if (jump) Jump();
    }
    
    protected virtual void Inputs()
    {

    }

    protected void Movement()
    {
        Vector2 _move = Vector2.right * speed * x;
        _move.y = rb.velocity.y;
        rb.velocity = _move;
    }
    protected void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0f);
        rb.AddForce(Vector2.up * jumpSpeed);
        animator.SetBool("Jump", true);
        jump = false;
    }
    protected bool ItsGround()
    {
        return Physics2D.OverlapCircle(posfoot.position, radius, layerGround);
    }

    
    protected void Virar()
    {
        if ((x > 0 && olhandoEsquerda) ||
            (x < 0 && !olhandoEsquerda))
        {
            Vector3 _scale = transform.localScale;
            _scale.x *= -1f;
            olhandoEsquerda = !olhandoEsquerda;
            transform.localScale = _scale;
        }
    }
    protected void Animation1()
    {
        
        if (Input.GetAxis("Horizontal") != 0)
        {
            animator.SetBool("Walking", true);
        }
        else
        {
            animator.SetBool("Walking", false);

        }

    } 
    protected void Animation2()
    {
        
        if (Input.GetAxis("Horizontal2") != 0)
        {
            animator.SetBool("Walking", true);
        }
        else
        {
            animator.SetBool("Walking", false);

        }

    }
    public void TakesDamage()
    {
        GetComponent<LifeController1>().TakesDamege(20);
    }
    
    protected virtual void Death()
    {
        GetComponent<CapsuleCollider2D>().enabled = false;
        rb.isKinematic = true;

        if (dead)
        {
            GameController.instance.gameOver.SetActive(true);
            animator.SetBool("Death", dead);
        }

    }
}
