using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ully : PlayerController1
{
    [Header("Sistema de Ataque - Ully")]
    [SerializeField] private float attackRange;
    [SerializeField] private float nextAttack;
    [SerializeField] private float attackCadence;
    private float distance;
    bool attack;

    [SerializeField] Transform posAttack;
    [SerializeField] float radiusAttack;
    [SerializeField] LayerMask enemyLayer;


    protected override void Inputs()
    {
        base.Inputs();

        x = Input.GetAxisRaw("Horizontal");

        if (Input.GetKeyDown(KeyCode.W) && ItsGround())
        {
            jump = true;
            AudioController.instance.JumpP1.Play();
        }

        if (ItsGround() && rb.velocity.y == 0) animator.SetBool("Jump", false);

        if (Input.GetKeyDown(KeyCode.G))
        {
            StartCoroutine(ATtack());
            
        }
        Animation1();
    }
       
    
    IEnumerator ATtack()
    {
        if (Time.time >= nextAttack)
        {
            AudioController.instance.AttackP1.Play();
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(0.25f);
            Debug.Log("atacar");
            nextAttack = Time.time + attackCadence;
            Collider2D[] alvos = Physics2D.OverlapCircleAll(posAttack.position, radiusAttack, enemyLayer);

            if (alvos.Length > 0)
            {
                foreach (var alvo in alvos)
                {
                    Debug.Log("Acertou: " + alvo.name);
                    Destroy(alvo.gameObject);
                    Debug.Log("Atacou");
                }
            }

        }
    }

    protected override void Death()
    {
        base.Death();
        if (dead)
        {
            GameController.instance.gameOver.SetActive(true);
            animator.SetBool("Death", dead);
        }
    }
}
