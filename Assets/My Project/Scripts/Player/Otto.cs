using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Otto : PlayerController1
{


    [Header("Sistema de Ataque - Otto")]
    public GameObject shotProjetil;
    public Transform bow;
    private bool shot;
    public float shotPower;
    public float cd;
    public float nextShot;


    protected override void Inputs()
    {
        base.Inputs();

        x = Input.GetAxisRaw("Horizontal2");
        shot = Input.GetKeyDown(KeyCode.L);

        if (Input.GetKeyDown(KeyCode.UpArrow) && ItsGround())
        {
            jump = true;
            AudioController.instance.JumpP2.Play();
        }
        if (ItsGround() && rb.velocity.y == 0) animator.SetBool("Jump", false);

        if (Input.GetKeyDown(KeyCode.L))
        {

            Atirar();
            
        }

        Animation2();
    }
    



    void Atirar()
    {
        if (Time.time >= nextShot && shot)
        {
            AudioController.instance.AttackP2.Play();
            animator.SetTrigger("Attack");
            GameObject tiro = Instantiate(shotProjetil, bow.position, bow.rotation);
            tiro.transform.position = bow.position;
            float _x = transform.localScale.x;
            nextShot = Time.time + cd;
            if (_x > 0)
            {
                Vector3 _scale = tiro.transform.localScale;
                _scale.x = 1f;
                tiro.transform.localScale = _scale;
                tiro.GetComponent<Rigidbody2D>().velocity = new Vector2(shotPower, 0f);
            }
            else
            {
                Vector3 _scale = tiro.transform.localScale;
                _scale.x = -1f;
                tiro.transform.localScale = _scale;
                tiro.GetComponent<Rigidbody2D>().velocity = new Vector2(-shotPower, 0f);
            }
            Destroy(tiro.gameObject, 1);
        }

    }
}
